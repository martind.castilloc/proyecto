
public class Persona {
	
	private Virus disease;
	private int id = 0;
	private String status = "Sano";	
	private int tiempo = 0;
	private Double probabilidad_de_muerte = 0.0;
	
	Persona (Virus disease) {
		this.disease = disease;
	}
	
	public void setId (int id) {
		this.id = id;
	}
	
	public int getId () {
		return this.id;
	}
	
	public void setStatus (String status) {
		this.status = status;
	}
	
	public String getStatus () {
		return this.status;
	}
	
	public int getDia_infectado () {
		return this.tiempo;
	}
	
	public void comprobar() {
		
			if (this.status == "Infectado") {
			
		
				if (this.tiempo < disease.getInfeccion()) {
					int estado_del_enfermo = (int)(Math.random()*2+1);
								
		
				if (estado_del_enfermo == 1) {
					this.probabilidad_de_muerte += this.disease.getMuertes();
				}
				
				 
				if ((Math.random()*1) < probabilidad_de_muerte) {
					this.setStatus("Muerto");
					this.disease.eliminar_caso();
				}
				
				
				this.tiempo += 1;
			}
			
		
			else if (this.tiempo >= this.disease.getInfeccion()) {
				this.setStatus("Sano");			
				this.disease.eliminar_caso();
			}
		}
	}
}
