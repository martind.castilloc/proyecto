/* Lucas Casanova
 * Vicente Chandia
 * Martin Castillo
 */

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
					
		Virus enfermedad = new Virus(0.3, 14, 0.15);	
			
		Comunidad c = new Comunidad(2000000, 1000, 50000, 0.9, enfermedad);		
		
		c.poblacion_inicial();
		
		Simulacion simulation = new Simulacion();
		        		    
		simulation.addComunidad(c);
		
		simulation.run(46);		
	}
	
	 
}
