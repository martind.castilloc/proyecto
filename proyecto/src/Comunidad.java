import java.util.ArrayList;

public class Comunidad {
	
	private int Contacto;
	private Double pro_contacto;
	private Virus enfermedad;	
	private int poblacion;
	private int infectado;
	private int Total_Infectados;	
	
	private ArrayList<Persona> gente = new ArrayList<Persona>();
	
	Comunidad (int poblacion, int infectado, int Contacto, Double pro_contacto, Virus enfermedad) {
		
		this.poblacion = poblacion;
		this.infectado = infectado;
		this.Total_Infectados = infectado;
		this.Contacto = Contacto;
		this.pro_contacto = pro_contacto;
		this.enfermedad = enfermedad;
	}
	

	public void poblacion_inicial () {
				
		for (int i=1; i < this.poblacion+1; i++) {
		
			gente.add(new Persona(this.enfermedad));
			gente.get(i-1).setId(i);
			
			}
		
		
		for (int j=1; j <= this.infectado; j++) {
			
			Boolean contagio = false;		
			while(contagio == false) {
		
				int subject = (int)(Math.random()*this.poblacion);
										
				if (gente.get(subject).getStatus() != "Infectado") {					
				
					gente.get(subject).setStatus("Infectado");
					this.enfermedad.addCaso_activo();
					contagio = true;
					
				}
			}
		}
	}
	
	
			
	public void paso () {

		Double Contacto = (Math.random()*1);
		if (Contacto <= pro_contacto) {
					
			for (int i = 0; i < this.Contacto; i++) {
			
				Boolean infectar = false;
				while (infectar == false) {
							
					
					int persona_x = (int)(Math.random()*this.poblacion);
					
										
					if (gente.get(persona_x).getStatus() != "Infectado") {
						
																
						if (this.enfermedad.accion_infectar(gente.get(persona_x))) {											
								gente.get(persona_x).setStatus("Infectado");
								
								this.enfermedad.addCaso_activo();
								this.Total_Infectados  += 1;
							}
							infectar = true;					
					}
				}
			}
			
		}
		
		
		for (int i = 0; i < gente.size(); i++) {
			gente.get(i).comprobar();
		}
	}
	
	
	public void informacion (int step) {
		System.out.print("[Paso "+step+"] ");
		System.out.print("Total de contagios de la comunidad 1: "+Total_Infectados );
		System.out.print(" Casos activos: "+this.enfermedad.getCasosactivos());
		
		System.out.println("");
	}
		

	public Virus getEnfermedad () {
		return this.enfermedad;
	}
	
}
