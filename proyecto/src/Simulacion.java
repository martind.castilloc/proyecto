
public class Simulacion {
	
	private Comunidad simulacion;
	
	Simulacion () {}
	
	
	public void addComunidad (Comunidad prueba) {
		this.simulacion = prueba;
	}
	
	
	public void run (int steps) {
		for (int i = 1; i < steps +1; i++) {
			
			simulacion.informacion(i);
			
			if (simulacion.getEnfermedad().getCasosactivos() != 0) {
				simulacion.paso();
			}
			
		}
	}
}
